
/**
 * Simple representation of calendar dates.
 * 
 * <p>Date objects are immutable. They can be compared, printed, their components examined, and they can be translated to/from String representations.</p>
 * <p>Given a Date object, the day of the week on which it falls can be obtained.</p>
 * <p>Given two Date objects that represent a period of time, the number of days in that period can be obtained.</p>
 * 
 * @author Stephan Jamieson
 * @version 12/8/2015
 */
public class Date implements Comparable<Date> {

    private final int day;
    private final int month;
    private final int year;
    private final int dayOfWeek;
    
    /**
     * Create a Date object to represent the given day in the given month and year.
     */
    public Date(final int day, final int month, final int year) {
        if (!Date.isValidDate(day, month, year)) {
            throw new IllegalArgumentException("Date("+day+", "+month+", "+year+"): arguments do not form a valid date.");
        }
        else {
            this.day=day;
            this.month=month;
            this.year=year;
            this.dayOfWeek = Date.dayOfWeek(day, month, year);
        }
    }
    
    /**
     * Create a Date object to represent the given date.
     * @param date a String with the following format: &lt;d&gt;&lt;d&gt;/&lt;m&gt;&lt;m&gt;/&lt;y&gt;&lt;y&gt;&lt;y&gt;&lt;y&gt;.
     */
    public Date(final String date) {
        final IllegalArgumentException illExcep = new IllegalArgumentException("Date("+date+"): argument format is incorrect.");
        String[] parts=date.split("/");
        if (parts.length==3) { 
            try {
                this.day=Integer.parseInt(parts[0]);
                this.month=Integer.parseInt(parts[1]);
                this.year=Integer.parseInt(parts[2]);
            }
            catch (Exception excep) {
                throw illExcep;
            }
            if (!Date.isValidDate(this.day, this.month, this.year)) {
                throw illExcep;
            }
            this.dayOfWeek = Date.dayOfWeek(day, month, year);
        }
        else {
            throw illExcep;
        }
    }
    
    /**
     * Obtain the day of the month.
     */
    public int day() { return day; }
    
    /**
     * Obtain the month of the  year.
     * @return a value in the range 1 to 12 inclusive, where 1 is January and 12 is December.
     */
    public int month() { return month; }
    /**
     * Obtain the year (a 4 digit number).
     */
    public int year() { return year; }
    
    /**
     * Obtain the day of the week on which this date falls.
     * @return a value in the range 1 to 7, where 1 is Sunday, 2 is Monday, ..., and 7 is Saturday.
     */
    public int dayOfWeek() { return Date.dayOfWeek(this); }
    
    /**
     * Obtain the calendar date that immediately follows this one.
     */
    public Date next() {
        int day=this.day()+1;
        int month=this.month();
        int year=this.year();
        if (day>daysInMonth(this.month(), this.year())) {
            day=1;
            month=month+1;
        }
        if (month>12) {
            month=1;
            year=year+1;
        }
        return new Date(day, month, year);
    }

    /**
     * Obtain the number of days between this Date (exclusive) and the other Date (inclusive).
     * <p>e.g. Given Date objects representing 1/1/2015 and 10/1/2015, this method will return the value 9.</p>
     */
    public int duration(final Date other) {
        if (this.year()==other.year()) {
            return Math.abs(Date.dayOfYear(this.day(), this.month(), this.year())-Date.dayOfYear(other.day(), other.month(), other.year()));
        }
        else {
            Date dateOne = null;
            Date dateTwo = null;
            if (this.year()<other.year()) { dateOne = this; dateTwo = other; } else { dateOne = other; dateTwo = this; }
            int days = Date.daysInYear(dateOne.year())-Date.dayOfYear(dateOne)+Date.dayOfYear(dateTwo);
            for(int year=dateOne.year()+1; year<dateTwo.year(); year++) { days = days+daysInYear(year);}
            return days;
        }
    }
    
    /**
     * Determine whether Object o is a Date and is equivalent to this Date i.e. has the same day, month, year.
     */
    public boolean equals(final Object o) {
        if (!(o instanceof Date)) {
            return false;
        }
        else {
            Date other = (Date)o;
            return this.day()==other.day()&&this.month()==other.month()&&this.year()==other.year();
        }
    }
    
    /**
     * Obtain a hashcode value for this Date object.
     * <p>Date objects may be used in hashed data collections.</p>
     */
    @Override public int hashCode() {
        int result = 17;
        result = result*31+this.day();
        result = result*31+this.month();
        result = result*31+this.year();
        return result;
    }
    
    /**
     * Compare this Date to the other Date.
     * <p>Return a -ve value if this Date precedes the other Date.</p>
     * <p>Return a 0 value if this Date is equivalent to the other Date i.e. has the same day, month, year.</p>
     * <p>Return a +ve value if the other Date precedes this Date.</p>
     */
    public int compareTo(final Date other) {
        int result = this.year()-other.year();
        if (result!=0) { return result; }
        result = this.month()-other.month();
        if (result!=0) { return result; }
        result = this.day()-other.day();
        return result;
    }
    
    /**
     * Obtain a String representation of this date in the following format: &lt;d&gt;&lt;d&gt;/&lt;m&gt;&lt;m&gt;/&lt;y&gt;&lt;y&gt;&lt;y&gt;&lt;y&gt;.
     */
    public String toString() {
        return String.format("%02d/%02d/%4d", day, month, year);
    }
    
    /* Class methods */
    /**
     * Determine whether the given year is a leap year.
     */
    public static boolean isLeapYear(final int year) { 
        assert(year>=0);
        return year%4==0&&year%100!=0||year%400==0; 
    }
    
    /**
     * Obtain the number of days in <code>date.year()</code>.
     */
    public static int daysInYear(final Date date) { return Date.daysInYear(date.year()); }
    
    /**
     * Obtain the number of days in the given year.
     */
    public static int daysInYear(final int year) {
        return Date.isLeapYear(year) ? 366 : 365;
    }
    
    /**
     * Determine whether the given values represent a calendar date.
     */
    public static boolean isValidDate(final int day, final int month, final int year) {
        return year>=0&&month>0&&month<13&&day>0&&day<=Date.daysInMonth(month, year);
    }
        
    private static int[][] months = { {31, 0}, {28, 31}, {31, 59}, {30, 90}, {31, 120}, {30, 151}, {31, 181}, {31, 212}, {30, 243}, {31, 273}, {30, 304}, {31, 334} };

    /**
     * Determine the number of days in the month and year of the given date i.e. <code>daysInMonth(date.month(), date.year())</code>. 
     */
    public static int daysInMonth(final Date date) { return Date.daysInMonth(date.month(), date.year()); }
    
    /**
     * Determine the number of days in the given month of the given year.
     */
    public static int daysInMonth(final int month, final int year) {
        assert(month>0&&month<13&&year>=0);
        if (month==2&&Date.isLeapYear(year)) {
            return months[month-1][0]+1;
        }
        else {
            return months[month-1][0];
        }
    }

    /**
     * Determine the day of the year represented by the given date.
     * <p>e.g. 1/1/2015 is the 1st day of 2015 and 31/12/2015 is the 365th day of 2015.</p> 
     */
    public static int dayOfYear(final Date date) { return Date.dayOfYear(date.day(), date.month(), date.year()); }
    
    /** 
     * Determine the day of the given year represented by the given day of the given month of that year.
     * <p>e.g. 1/1/2015 is the 1st day of 2015 and 31/12/2015 is the 365th day of 2015.</p> 
     */
    public static int dayOfYear(final int day, final int month, final int year) {
        int days = day+months[month-1][1];
        if (Date.isLeapYear(year)&&month>2) { days++; }
        return days;
    }

    /**
     * Obtain the day of the week on which January 1st of the given year falls.
     * @return a value in the range 1 to 7, where 1 is Sunday, 2 is Monday, ..., and 7 is Saturday.
     */
    public static int dayOfJanuaryFirst(final int year) {
        return (1+5*((year-1)%4)+4*((year-1)%100)+6*((year-1)%400))%7+1;
    }
    
    /**
     * Obtain the day of the week on which the given date falls.
     * @return a value in the range 1 to 7, where 1 is Sunday, 2 is Monday, ..., and 7 is Saturday.
     */
    public static int dayOfWeek(final Date date) { return Date.dayOfWeek(date.day(), date.month(), date.year()); }

    /**
     * Obtain the day of the week on which the given date falls.
     * @return a value in the range 1 to 7, where 1 is Sunday, 2 is Monday, ..., and 7 is Saturday.
     */    
    public static int dayOfWeek(final int day, final int month, final int year) {
        return (dayOfJanuaryFirst(year)-1+Date.dayOfYear(day, month, year)-1)%7+1;
    }
}
