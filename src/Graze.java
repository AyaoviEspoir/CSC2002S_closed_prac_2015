/*
Started on the 19/08/2015
File : Graze.java
By Ayaovi Espoir Djissenou
*/

import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

public class Graze
{
	public static Map<String, ArrayList<String>> _trackingData;

	public static ArrayList<String> _result;

	public Graze()
	{
		_trackingData = new HashMap<String, ArrayList<String>>();

		_result = new ArrayList<String>();
	}

	public static void main(String args[])
	{
		Graze graze = new Graze();

		Utilities.readData("../res/tracks.dat");

		System.out.println("THE SIZE OF DATA IS : " + _trackingData.size());

		for (String UID : _trackingData.keySet())
		{
			searchForSubsequences(UID);
		}

		sortResult();

		System.out.println("SIZE OF RESULT : " + _result.size());

		Utilities.writeToFile("../res/myResult.txt");
	}

	/*
	Sorts the result array with regards to date.
	*/
	private static void sortResult()
	{
		String previousData[];

		String currentData[];

		String previousDate[];

		String currentDate[];

		for (int i = 1; i < _result.size(); i++)
		{
			previousData = _result.get(i - 1).split(" ");

			currentData = _result.get(i).split(" ");

			previousDate = previousData[1].split("/");

			currentDate = currentData[1].split("/");

			for (int j = 2; j >= 0; j--)
			{
				if (Integer.parseInt(currentDate[j]) < Integer.parseInt(previousDate[j]))
				{
					String temp = _result.get(i - 1);

					String temp2 = _result.get(i);

					_result.remove(i - 1);

					_result.add((i - 1), temp2);	/* Swap position for current and previous, which would have been easier with a replace method. */

					_result.remove(i);

					_result.add(i, temp);

					break;
				}
			}
		}
	}

	private static void searchForSubsequences(String UID)
	{
		String data[] = _trackingData.get(UID).get(0).split(" ");		/* Returns the first data. */

		String startingDateOfSequence = data[0];

		String coordinates = data[1] + " " + data[2];

		Location previousLocation = new Location(coordinates);

		Location currentLocation = new Location(coordinates);

		int counter = 1;

		boolean resetStartingDate = false;				/* Decides whether or not to set the date to a new beginning. */

		for (int i = 1; i < _trackingData.get(UID).size(); i++)
		{
			data = _trackingData.get(UID).get(i).split(" ");

			coordinates = data[1] + " " + data[2];

			currentLocation.setCoordinates(coordinates);

			if (previousLocation.distance(currentLocation) <= 100.0)
			{
				++counter;
			}

			else
			{
				if (counter >= 3)
				{
					_result.add(UID + " " + startingDateOfSequence + " " + counter);
				}

				startingDateOfSequence = data[0];			/* Sets the date back to this current day. */

				counter = 1;
			}

			previousLocation.setCoordinates(coordinates);
		}

		if (counter >= 3)
		{
			_result.add(UID + " " + startingDateOfSequence + " " + counter);
		} 
	}	
}