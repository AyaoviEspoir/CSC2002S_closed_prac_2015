/**
 * An instance of this class represents a geographic location expressed as an integer coordinate.
 * 
 * <p>An origin (0,0) is assumed. North and East are positive. West and South are negative.</p>
 * 
 * <p>Given two locations, the distance between them can be obtained.</p>
 * <p>Given a start location, a bearing and a distance, the end location can be obtained.</p>
 * 
 * @author Stephan Jamieson
 * @version 12/8/2015
 */
public class Location {

    
    private int x;
    private int y;
    
    /**
     * Create a Location object for the given x and y ordinates expressed as a String with the format:
     * <integer><space><integer> e.g. "-50 59".
     * <p>Values can be positive or negative.</p>
     */
    public Location(final String coordinates) {
        
        setCoordinates(coordinates);
    }
    
        
        
    /**
     * Create a Location object for the given x and y ordinates.
     */
    public Location(final int x, final int y) {
        this.x = x;
        this.y = y;
    }

    /*
    Sets new coordinates of the Location.
    */
    public void setCoordinates(final String coordinates)
    {

        final String ERR_STRING =  "Location("+coordinates+"): String format error - ";
        
        final String[] values = coordinates.split(" ");
        
        if (values.length!=2) 
        { 
            throw new IllegalArgumentException(ERR_STRING+" incorrect number of parts.");
        }

        else 
        {
            try 
            {
                this.x = Integer.parseInt(values[0].trim());
            
                this.y = Integer.parseInt(values[1].trim());
            }
            
            catch (NumberFormatException numFormExcep) 
            {
                throw new IllegalArgumentException(ERR_STRING+" failed to convert to integer numbers.");
            }
        }
    }

    
    /**
     * Obtain the x axis component for this location.
     */
    public int getX() { return this.x; }
    /**
     * Obtain the y axis component for this location.
     */
    public int getY() { return this.y; }
    

    /**
     * Obtain the distance between this location and the given location. 
     */
    public double distance(Location other) {
        final double aSquare = Math.pow(this.getX()-other.getX(), 2);
        final double bSquare = Math.pow(this.getY()-other.getY(), 2);
        return Math.sqrt(aSquare+bSquare);
    }
    
    /**
     * Return the (approximate) location that is the given distance from this location on the given bearing (clockwise from North).
     */
    public Location destination(final double bearing, final double distance) {
        assert(bearing>=0&&bearing<360);
        final double angle = Math.toRadians(bearing);
        double xDistance = distance*Math.sin(angle);
        double yDistance = distance*Math.cos(angle);
        final int destX = (int)Math.rint(this.getX() + xDistance);
        final int destY = (int)Math.rint(this.getY() + yDistance);
        return new Location(destX, destY);
    }
    
    /**
     * Obtain a String representation of this location
     */
    public String toString() {
        return String.format("%d %d", this.getX(), this.getY());
    }

}
