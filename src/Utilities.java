import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Scanner;
import java.util.ArrayList;
import java.io.FileInputStream;
import java.io.FileNotFoundException; 

class Utilities
{

	public Utilities()
	{

	}

	/*
	Extracts the UID from the data.
	*/
	private static String extractUID(String data)
	{
		return data.substring(data.lastIndexOf(" ") + 1);
	}

	/*
	 * A method to read a file into an array of floats.
	 * @param Scanner inputStream, String data;
	 */
	public static void readData(String fileName) {
		
		try
		{
			Scanner inputStream = new Scanner(new FileInputStream(fileName));

			while (inputStream.hasNextLine()) 
			{	
				String line = inputStream.nextLine();

				String key = extractUID(line);

				String value = line.substring(0, (line.lastIndexOf(" ") + 1));

				if (Graze._trackingData.containsKey(key))
				{

					Graze._trackingData.get(key).add(value);
				}

				else
				{
					ArrayList<String> data = new ArrayList<String>();

					data.add(value);

					Graze._trackingData.put(key, data);
				}
			}

			inputStream.close();
		}

		catch (FileNotFoundException e)
		{
			System.out.println("File(s) not found or could not open file(s)");
		}
	}

    /*
    * method.
    */
	public static void writeToFile(String fileName) 
	{

		BufferedWriter bfwriter = null;

		try 
		{

			File myFile = new File(fileName);

			if (!myFile.exists())

				myFile.createNewFile();

			Writer writer = new FileWriter(myFile);

			bfwriter = new BufferedWriter(writer);

			for (int i = 0; i < Graze._result.size(); i++)
			{
				bfwriter.write(Graze._result.get(i));

				bfwriter.newLine();
			}
		}

		catch(IOException e)
		{
			e.printStackTrace();
		}

		finally{

			try
			{
				if (bfwriter!=null)

				bfwriter.close();
			}

			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	/*
	* A method to get the start time of a process.
	*/
	public static long tick()
	{	
		return System.nanoTime();	
	}
	
    /*
	 * A method to get the end time of a process.
	 */
	public static float toc(long startTime)
	{	
		return ((System.nanoTime() - startTime) / 1000000.0f); 	
	}
}